package com.eightafun.colorspin;

import com.eightafun.colorspin.ColorSpinView.ColorSpinThread;
import com.nineoldandroids.animation.ObjectAnimator;
import android.media.AudioManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


public class ColorSpinActivity extends Activity {

	
	private ColorSpinView view;
	
	private final String PREFERENCES = "PreferencesStore";
	private final String GAME_SAVE = "GameSave";
	private final String KEY_SOUND_MUTED = "keySoundMuted";
	private final String KEY_MUSIC_MUTED = "keyMusicMuted";
	private String KEY_NEW_SAVE = "keyNewSave";
	
	private View pauseMenu, mainMenu;
	private boolean soundMuted,musicMuted; 
	
	private OnClickListener listener = new OnClickListener() {
    	@Override
    	public void onClick(View v) {
    		if (v.getId() == R.id.pause_button) {
    			view.getThread().setState(ColorSpinView.ColorSpinThread.STATE_PAUSED);    			
    			Log.e("activity", "set thread state TO PAUSED");
    		}
    		else if (v.getId() == R.id.resume_button) {
    			view.getThread().setState(ColorSpinView.ColorSpinThread.STATE_RUNNING);
    			Log.e("activity", "SET THREAD state To RUNNING");
    		}
    		else if (v.getId() == R.id.return_to_menu_button || v.getId() == R.id.game_over_return_to_menu) {
    			view.getThread().setState(ColorSpinView.ColorSpinThread.STATE_MAIN_MENU);
    		}
    		else if (v.getId() == R.id.new_game) {
    			view.getThread().setState(ColorSpinView.ColorSpinThread.STATE_MAIN_MENU_TO_RUNNING_TRANSITION);
    		}
    		else if (v.getId() == R.id.play_again) {
    			view.getThread().newGameReset();
    			view.getThread().setState(ColorSpinView.ColorSpinThread.STATE_RUNNING);
    		}
    		else if (v.getId() == R.id.sound_mute) {
    			//update sound here
    			ImageButton sm = (ImageButton) v;
    			if (soundMuted) {
    				sm.setImageResource(R.drawable.sound_icon_unselected);
    				soundMuted = false;
    				view.setSoundMute(false);
    			}
    			else{
    				sm.setImageResource(R.drawable.sound_icon_selected);
    				soundMuted = true;
    				view.setSoundMute(true);
    			}
    		}
    		else if (v.getId() == R.id.music_mute) {
    			//update sound here
    			ImageButton sm = (ImageButton) v;
    			if (musicMuted) {
    				sm.setImageResource(R.drawable.music_icon_unselected);
    				musicMuted = false;
    				view.setMusicMute(false);
    			}
    			else {
    				sm.setImageResource(R.drawable.music_icon_selected);
    				musicMuted = true;
    				view.setMusicMute(true);
    			}
    		}
    	}
    };
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("activity", "created activity");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.colorspin_layout);
        Log.e("activity", "set content view");
        
        view = (ColorSpinView) findViewById(R.id.colorspinview);
        
        pauseMenu = findViewById(R.id.pause_menu);
        mainMenu = findViewById(R.id.main_menu);
        view.setPauseMenu(pauseMenu);
        view.setMainMenu(mainMenu);
        view.setScoreView((TextView)findViewById(R.id.score));
        view.setGameOverMenu(findViewById(R.id.game_over_menu));
        view.setSoundControls(findViewById(R.id.sound_controls));
        view.setSettingsMenu(findViewById(R.id.settings_menu));
        view.setGameOverScoreView((TextView)findViewById(R.id.game_over_score_view));
       
        ImageButton bu = (ImageButton) findViewById(R.id.pause_button);
        bu.setOnClickListener(listener);
        view.setPauseButton(bu);
        bu = (ImageButton) findViewById(R.id.sound_mute);
        bu.setOnClickListener(listener);
        bu = (ImageButton) findViewById(R.id.music_mute);
        bu.setOnClickListener(listener);
        Button b = null;
        b = (Button) findViewById(R.id.resume_button);
        b.setOnClickListener(listener);       
        b = (Button) findViewById(R.id.new_game);
        b.setOnClickListener(listener);
        b= (Button) findViewById(R.id.return_to_menu_button);
        b.setOnClickListener(listener);
        b = (Button) findViewById(R.id.play_again);
        b.setOnClickListener(listener);
        b = (Button) findViewById(R.id.game_over_return_to_menu);
        b.setOnClickListener(listener);
               
    }
    @Override
    protected void onStart() {
    	super.onStart();
    	Log.e("activity", "activity on start");
    }
    
    @Override
    protected void onResume() {
    	super.onResume(); 
    	Log.e("activity", "Activity resume"); 
    	AudioManager mgr = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        int i = mgr.requestAudioFocus(view, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (i == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
        	view.hasAudioFocus = true;
        }
        Log.e("activity", "result of asking for focus is "+i);
    		
    	view.activityResume();
    	SharedPreferences prefs = getSharedPreferences(GAME_SAVE, 0);
    	if (prefs.getBoolean(KEY_NEW_SAVE, false)) {
    		if (prefs.getBoolean(KEY_SOUND_MUTED, false)) {
    			((ImageButton)findViewById(R.id.sound_mute)).setImageResource(R.drawable.sound_icon_selected);
    		}
    		if (prefs.getBoolean(KEY_MUSIC_MUTED, false)) {
    			((ImageButton)findViewById(R.id.music_mute)).setImageResource(R.drawable.music_icon_selected);
    		}
    		view.restoreThread(prefs);
    		view.requestThreadStart();
    		prefs.edit().putBoolean(KEY_NEW_SAVE, false).commit();
    		Log.e("activity", "restoring a save");
    	}
    	else {
    		view.requestThreadStart();
    		view.getThread().setState(ColorSpinView.ColorSpinThread.STATE_MAIN_MENU); 
    		Log.e("activity", "starting afresh");
    	}
    	
    }
    @Override
	protected void onPause() {
    	super.onPause();
    	if (view.getThread().getCurrentState() == ColorSpinView.ColorSpinThread.STATE_RUNNING) {
    		view.getThread().setState(ColorSpinView.ColorSpinThread.STATE_PAUSED);
    	}
    	SharedPreferences.Editor editor = getSharedPreferences(GAME_SAVE, 0).edit(); //the commit is done in the thread, no need to worry about this warning
    	view.saveThread(editor);
    	view.getThread().setRunning(false);
    	view.releaseMediaPlayers();
    	Log.e("activity", "Activity pause");    	  
    }    
    
    @Override 
    protected void onStop() {
    	super.onStop();
    	AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    	mgr.abandonAudioFocus(view);
    	Log.e("activity", "Activity stop");
    }
}
