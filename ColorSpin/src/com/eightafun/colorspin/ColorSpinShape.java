package com.eightafun.colorspin;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

public class ColorSpinShape {
	
	private float angle;
	private float dist;
	private int color;
	private int colorWheelRadius;
	private static final int YELLOW_LEFT = 248, YELLOW_RIGHT = 352, PURPLE_LEFT = 8, PURPLE_RIGHT = 112, BLUE_LEFT = 128, BLUE_RIGHT = 232;
	public static final int COLOR_YELLOW = 0, COLOR_PURPLE = 1, COLOR_BLUE = 2;
	public static final int STATE_TRAVEL = 0, STATE_GOOD = 1, STATE_BAD = 2, STATE_GAME_END = 3;
	private int currentState = STATE_TRAVEL;
	private int alpha=255;
	private int alphaUpDownCounter;
	private int ALPHA_RATE = 1000;
	private Paint paint = new Paint();
	private RectF scratchRect;
	private int windowWidth, windowHeight;
	private boolean isDone;
	private boolean playSound;
	
	public ColorSpinShape(float ang, int col, int wWidth, int wHeight, int radius) {
		angle = ang;
		color = col;
		windowWidth = wWidth;
		windowHeight = wHeight;
		dist = (float) (Math.sqrt(Math.pow(windowWidth/2,2)+Math.pow(windowHeight/2,2))+30);
		colorWheelRadius = radius;
		if (color == COLOR_YELLOW) {
			paint.setARGB(255,245, 235, 24);
		}
		else if (color == COLOR_PURPLE) {
			paint.setARGB(255,255,13,249);
		}
		else if (color == COLOR_BLUE) {
			paint.setARGB(255,63,72,204);
		}
		scratchRect = new RectF(windowWidth/2-colorWheelRadius/4, windowHeight/2-colorWheelRadius/4, windowWidth/2+colorWheelRadius/4, windowHeight/2+colorWheelRadius/4);
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
	}
	public ColorSpinShape(int restoreState, int restoreColor, float restoreAngle, float restoreDistance, int restoreAlpha, int restoreAlphaUpDownCounter, boolean restoreIsDone, int wWidth, int wHeight, int radius) { //used for restoring from a string
		currentState = restoreState;
		color = restoreColor;
		angle = restoreAngle;
		dist = restoreDistance;
		alpha = restoreAlpha;
		alphaUpDownCounter = restoreAlphaUpDownCounter;
		isDone = restoreIsDone;
		windowWidth = wWidth;
		windowHeight = wHeight;		
		colorWheelRadius = radius;
		if (color == COLOR_YELLOW) {
			paint.setARGB(255,245, 235, 24);
		}
		else if (color == COLOR_PURPLE) {
			paint.setARGB(255,255,13,249);
		}
		else if (color == COLOR_BLUE) {
			paint.setARGB(255,63,72,204);
		}
		scratchRect = new RectF(windowWidth/2-colorWheelRadius/4, windowHeight/2-colorWheelRadius/4, windowWidth/2+colorWheelRadius/4, windowHeight/2+colorWheelRadius/4);
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
		paint.setAlpha(alpha);
	}
	
	public float getDistance() {
		return dist;
	}
	public int getColor() {
		return color;
	}
	public float getAngle() {
		return angle;
	}
	public int getState() {
		return currentState;
	}
	public int update(long elapsed, float speed, float colorWheelAngle) {
		if (isDone) {
			return currentState;
		}
		if (currentState == STATE_TRAVEL) {
			dist -= ((float)elapsed/1000.0)*speed;
			
			if (dist < colorWheelRadius*1.25-1) {
				dist = colorWheelRadius*1.25f-1;
				
				float newLeft=0, newRight=0;
				if (color == COLOR_YELLOW) {
					newLeft = YELLOW_LEFT+colorWheelAngle;
					newRight = YELLOW_RIGHT+colorWheelAngle;
				}
				else if (color == COLOR_PURPLE) {
					newLeft = PURPLE_LEFT+colorWheelAngle;
					newRight = PURPLE_RIGHT+colorWheelAngle;
				}
				else if (color == COLOR_BLUE) {
					newLeft = BLUE_LEFT+colorWheelAngle;
					newRight = BLUE_RIGHT+colorWheelAngle;
				}
				if (newLeft >= 360) {
					newLeft-= 360;
				}
				if (newRight >= 360) {
					newRight -=360;
				}
				
				float margin = 360-newLeft;
				float newAngle = angle+margin;
				float bound = newRight+margin;
				if (newAngle >= 360) {
					newAngle -= 360;
				}
				if (bound >= 360) {
					bound-=360;
				}
					
				if (!(newAngle < bound)) {
					currentState = STATE_BAD;
					//Log.e("activity", "angle="+angle+"newLeft="+newLeft+"newRight"+newRight);
				}
				else {
					currentState = STATE_GOOD;
					playSound = true;
				}
			}
		}
		else if (currentState == STATE_GOOD) {
			playSound = false;
			alpha -= (((float)elapsed)/1000.0)*ALPHA_RATE;
			if (alpha < 0) {
				alpha = 0;
				isDone = true;				
			}
			paint.setAlpha(alpha);
		}
		else if (currentState == STATE_GAME_END) {
			alpha -= (((float)elapsed)/1000.0)*ALPHA_RATE;
			if (alpha < 0) {
				alpha = 0;
				isDone = true;				
			}
			paint.setAlpha(alpha);
		}
		else if (currentState == STATE_BAD) {
			if (alphaUpDownCounter == 7) {
				isDone = true;
				alpha = 0;
			}
			else if (alphaUpDownCounter % 2 == 0) {
				alpha -= ((float)elapsed)/1000.0*ALPHA_RATE;
				if (alpha < 0) {
					alpha = 0;
					alphaUpDownCounter++;
				}
			}
			else {
				alpha += ((float)elapsed)/1000.0*ALPHA_RATE;
				if (alpha > 255) {
					alpha = 255;
					alphaUpDownCounter++;
				}
			}
			paint.setAlpha(alpha);
		}
		return currentState;		 
	}
	public void draw(Canvas c) {
		c.save();
		c.rotate(angle, windowWidth/2, windowHeight/2);
		c.translate(0,-dist);
		c.drawRect(scratchRect, paint);	
		c.restore();
	}
	public boolean isDone() {
		return isDone;
	}
	public void restartShape(float ang, int col) {
		isDone = false;
		angle = ang;
		color = col;
		if (color == COLOR_YELLOW) {
			paint.setARGB(255,245, 235, 24);
		}
		else if (color == COLOR_PURPLE) {
			paint.setARGB(255,255,13,249);
		}
		else if (color == COLOR_BLUE) {
			paint.setARGB(255,63,72,204);
		}
		currentState = STATE_TRAVEL;
		alpha = 255;
		dist = (float) (Math.sqrt(Math.pow(windowWidth/2,2)+Math.pow(windowHeight/2,2))+30);
		alphaUpDownCounter = 0;
	}
	public void gameEnd() {
		if (currentState == STATE_BAD) {
			Log.e("activity", "shapestate exception already bad shape");
			return;
		}
		if (currentState != STATE_BAD || currentState != STATE_GAME_END) {
			currentState = STATE_GAME_END;
		}
		
	}
	public String saveToString() {
		//Save format, things are separated by pipes
		// state|color|angle|distance|alpha|alphaUpDownCounter|isDone|windowWidth|windowHeight|colorWheelRadius
		String saveString = ""+currentState+"|"+color+"|"+angle+"|"+dist+"|"+alpha+"|"+alphaUpDownCounter+"|"+isDone+"|"+windowWidth+"|"+windowHeight+"|"+colorWheelRadius;
		Log.e("activity","shape savestring="+saveString);
		return saveString;
	}
	public static ColorSpinShape restoreFromString(String restoreString) {
		String[] parameters = restoreString.split("\\|");
		Log.e("activity", "restoring shape parameters array length = "+parameters.length);
		Log.e("activity", "restoring shape parameters input string"+restoreString);
		try {
			int restoreState = Integer.parseInt(parameters[0]);
			int restoreColor = Integer.parseInt(parameters[1]);
			float restoreAngle = Float.parseFloat(parameters[2]);
			float restoreDistance = Float.parseFloat(parameters[3]);
			int restoreAlpha = Integer.parseInt(parameters[4]);
			int restoreAlphaUpDownCounter = Integer.parseInt(parameters[5]);
			boolean restoreIsDone = Boolean.parseBoolean(parameters[6]);
			int restoreWindowWidth = Integer.parseInt(parameters[7]);
			int restoreWindowHeight = Integer.parseInt(parameters[8]);
			int restoreColorWheelRadius = Integer.parseInt(parameters[9]);
			return new ColorSpinShape(restoreState, restoreColor, restoreAngle, restoreDistance, restoreAlpha, restoreAlphaUpDownCounter, restoreIsDone, restoreWindowWidth, restoreWindowHeight, restoreColorWheelRadius);
		}
		catch (Exception e) {
			return null;
		}
	}
	public boolean shouldPlaySound() {
		return playSound;
	}
}
