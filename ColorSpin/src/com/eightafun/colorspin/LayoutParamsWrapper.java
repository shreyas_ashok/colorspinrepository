package com.eightafun.colorspin;

import android.view.ViewGroup.MarginLayoutParams;

public class LayoutParamsWrapper {
	private MarginLayoutParams param;
	private int initialLeftMargin, initialTopMargin, initialBottomMargin;
	public LayoutParamsWrapper(MarginLayoutParams p) {
		param = p;
		initialLeftMargin = p.leftMargin;
		initialTopMargin = p.topMargin;
		initialBottomMargin = p.bottomMargin;
	}
	public float getLeftMargin() {
		return (float) param.leftMargin;
	}
	public void setLeftMargin(float f) {
		param.leftMargin = (int)f;
	}
	public float getTopMargin() {
		return (float) param.topMargin;
	}
	public void setTopMargin(float f) {
		param.bottomMargin += param.topMargin- (int)f;
		param.topMargin = (int) f;
		
	}
	public void setBottomMargin(float f) {
		param.bottomMargin = (int)f;
	}
	public float getBottomMargin() {
		return param.bottomMargin;
	}
	public float getInitialBottomMargin() {
		return initialBottomMargin;
	}
	public float getInitialLeftMargin() {
		return (float) initialLeftMargin;
	}
	public float getInitialTopMargin() {
		return (float) initialTopMargin;
	}
}
