package com.eightafun.colorspin;

import java.util.ArrayList;
import java.util.Random;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;

public class ColorSpinView extends SurfaceView implements SurfaceHolder.Callback, AudioManager.OnAudioFocusChangeListener {
	
	private Context mContext;
	private ColorSpinThread thread;
	private MediaPlayer startMusicPlayer, loopMusicPlayer, endNoisePlayer, shapeSoundPlayer;
	private boolean soundMuted, musicMuted;
	public boolean hasAudioFocus, mediaPlayersInitialized;
	private boolean threadWantsMusic;
	
	private final Handler scoreViewHandler = new Handler() { //no memory leak because we don't use postDelayed
		@Override
		public void handleMessage(Message m) {
			scoreView.setText(m.getData().getString("text"));
			gameOverScoreView.setText(m.getData().getString("gameOverText"));
		}
	};
	
	private final Handler stateHandler = new Handler() {
		@Override
		public void handleMessage(Message m) {
			thread.setState(m.getData().getInt("state"));
		}
	};
	
	private boolean surfaceReady, startRequested;
	private boolean pauseMenuUpRequested, pauseMenuDownRequested;
	private boolean mainMenuOutRequested, mainMenuInRequested;
	private boolean pauseButtonShowRequested, pauseButtonHideRequested;
	private boolean scoreViewShowRequested, scoreViewHideRequested;
	private boolean gameOverMenuInRequested, gameOverMenuOutRequested;
	
	private final String KEY_ANGLE = "keyAngle";
	private final String KEY_NEW_SAVE = "keyNewSave";
	private final String KEY_STATE = "keyState";
	private final String KEY_SHAPES = "keyShapes";
	private final String KEY_NEW_SHAPE_TIME = "keyNewShapeTime";
	private final String KEY_SCORE = "keyScore";
	private final String KEY_LOOP_MUSIC_TIME = "keyLoopMusicTime";
	private final String KEY_START_MUSIC_TIME = "keyStartMusicTime";
	private final String KEY_SOUND_MUTED = "keySoundMuted";
	private final String KEY_MUSIC_MUTED = "keyMusicMuted";
	
	private View pauseMenu, mainMenu, pauseButton, gameOverMenu, soundControls;
	private TextView scoreView, gameOverScoreView; 
	private LayoutParamsWrapper pauseMenuWrapper, mainMenuWrapper, pauseButtonWrapper, gameOverMenuWrapper, scoreViewWrapper, soundControlsWrapper;
	
	
	private boolean surfaceCreated;
	
	class ColorSpinThread extends Thread {
		
		private Random rand = new Random();
		
		private float lastTouchX = 0;
		private float lastTouchY = 0;
		private float touchX = 0;
		private float touchY = 0;
		
		private Drawable colorWheel;
		private int drawableWidth, drawableHeight;
		private volatile int colorWheelOffset;
		
		private float colorWheelAngle;
		private float currentSpeed=30.0f;
		private float SPEED_INCREASE_PER_MINUTE=50.0f;
		private float currentScore=0;
		private ArrayList<ColorSpinShape> shapes = new ArrayList<ColorSpinShape>();
		
		private long lastTime;
		private long nextShapeTime;
		
		private SurfaceHolder holder;
		private Context context;
		private Handler scoreHandler, stateSetHandler;
		
		private volatile int windowWidth = 0, windowHeight = 0;
		
		public static final int STATE_PAUSED = 0;
		public static final int STATE_RUNNING = 1;
		public static final int STATE_MAIN_MENU = 2;
		public static final int STATE_GAME_OVER = 3;
		public static final int STATE_GAME_OVER_TRANSITION = 4;
		public static final int STATE_MAIN_MENU_TO_RUNNING_TRANSITION = 5;
		
		private int currentState = STATE_PAUSED;
		private int mainMenuToRunningStateChangeTimer = 0;
		
		public static final int TOUCH_START = 0, TOUCH_MOVE = 1;
		
		private boolean isRunning = true;
		
		public ColorSpinThread(SurfaceHolder h, Context c, Handler scorer, Handler setStater) {
			holder = h;
			context = c;
			scoreHandler = scorer;
			stateSetHandler = setStater;
			
			Resources res = context.getResources();
			colorWheel = res.getDrawable(R.drawable.color_wheel);
			drawableWidth = colorWheel.getIntrinsicWidth();
			drawableHeight = colorWheel.getIntrinsicHeight();
		}
		
		
		
		public void setSurfaceSize(int width, int height) {
			synchronized(holder) {
				windowWidth = width;
				windowHeight = height;
				colorWheelOffset = windowWidth/16;
			}
		}
		public void setState(int state) {
			synchronized(holder) {
				currentState = state;
				if (currentState == STATE_PAUSED) {
					pauseMusicWithoutSweep();
					if (surfaceCreated) {
						pauseMenuUp();
						showSoundControls();
						showScoreView();
						//Log.e("activity", "animated pause menu up");
						//Log.e("activity", "vis"+pauseMenu.getVisibility());
					}
					else {
						pauseMenuUpRequested = true;
						scoreViewShowRequested = true;
						//Log.e("activity", "requested the pause menu up");
					}
				}
				else if (currentState != STATE_PAUSED) {
					if (surfaceCreated) {
						pauseMenuDown();
						hideSoundControls();
					}
					else {
						pauseMenuDownRequested = true;
					}
				}
				if (currentState != STATE_MAIN_MENU) {
					if (surfaceCreated) {
						mainMenuOut();
					}
					else {
						mainMenuOutRequested = true;
					}
				}
				else if (currentState == STATE_MAIN_MENU) {
					resetMusicToBeginning();
					startMusic();
					if (surfaceCreated) {
						mainMenuIn();
					}
					else {
						mainMenuInRequested = true;
					}
					newGameReset();
					lastTime = System.currentTimeMillis();
				}
				if (currentState == STATE_RUNNING) {
					startMusic();
					if (surfaceCreated) {
						showPauseButton();
						showScoreView();
					}
					else {
						pauseButtonShowRequested = true;
						scoreViewShowRequested = true;
					}
					lastTime = System.currentTimeMillis();
				}
				else if (currentState != STATE_RUNNING) {
					if (surfaceCreated) {
						hidePauseButton();
					}
					else {
						pauseButtonHideRequested = true;
					}
				}
				if (currentState != STATE_RUNNING && currentState != STATE_PAUSED) {
					if (surfaceCreated) {
						hideScoreView();
					}
					else {
						scoreViewHideRequested = true;
					}
				}
				if (currentState != STATE_GAME_OVER) {
					if (surfaceCreated) {
						gameOverMenuOut();
					}
					else {
						gameOverMenuOutRequested = true;
					}
				}
				if (currentState == STATE_GAME_OVER) {
					if (surfaceCreated) {
						gameOverMenuIn();
					}
					else {
						gameOverMenuInRequested = true;
					}
				}
				if (currentState == STATE_GAME_OVER_TRANSITION || currentState == STATE_MAIN_MENU_TO_RUNNING_TRANSITION) {
					endMusicWithSweep();
				}
					
				//Log.e("activity",""+currentState);
			}
		}
		public void run() {
			while(isRunning) {
				//Log.e("activity", "pausemenu translation-"+pauseMenu.getTranslationY());
				Canvas c = null;
				try {
					c = holder.lockCanvas(null);
					if (c != null) {
						synchronized(holder) {
							calculate();
							doDraw(c);						
						}
					}
					    
				}
				finally {
					if (c != null) {
						holder.unlockCanvasAndPost(c);
					}
				}
			}
		}
		
		public void calculate() {
			synchronized (holder) {
				if (currentState == STATE_RUNNING) {
					long elapsed = System.currentTimeMillis()-lastTime;
					if (elapsed > 0) {
						Log.e("activity", "FPS="+(1000/elapsed));
					}
					currentSpeed+= ((SPEED_INCREASE_PER_MINUTE/60f)/1000f)*elapsed;
					Log.e("activity", "speed"+currentSpeed);
					nextShapeTime+= elapsed;
					float initialAngle = 0;
					float newAngle = 0;
					if (lastTouchX == 0) { //FIRST Vector angle determination
						if (lastTouchY >=0) {
							initialAngle = 0;
						}
						else {
							initialAngle = 180;
						}
					}
					else {
						initialAngle = (float)Math.toDegrees(Math.atan((double)lastTouchY/(double)lastTouchX));
						initialAngle+=90;
						if (lastTouchX <0) {
							initialAngle+=180;
						}
						//Log.e("activity", "colorwheelangle"+colorWheelAngle);
					}
					
					
					if (touchX == 0) { //second vector angle determination
						if (touchY >=0) {
							newAngle = 0;
						}
						else {
							newAngle = 180;
						}
					}
					else {
						newAngle = (float)Math.toDegrees(Math.atan((double)touchY/(double)touchX));
						newAngle+=90;
						if (touchX <0) {
							newAngle+=180;
						}
						//Log.e("activity", ""+initialAngle);
					}
					colorWheelAngle += newAngle-initialAngle;
					while (colorWheelAngle > 360.0) {
						colorWheelAngle -= 360;
					}
					while (colorWheelAngle < 0.0) {
						colorWheelAngle += 360;
					}
					lastTouchX = touchX;
					lastTouchY = touchY;
					
					//shape stuff
					
					if (nextShapeTime > Math.max(2000-currentSpeed*4, 1000)) {
						nextShapeTime = 0;
						boolean updated = false;
						for (int i = 0; i < shapes.size(); i++) {
							ColorSpinShape shape = shapes.get(i);
							if (shape.isDone()) {
								shape.restartShape(rand.nextInt(360), rand.nextInt(3)); //second argument magic number allows the color to be randomized
								updated = true;
								break;
							}
						}
						if (!updated) {
							shapes.add(new ColorSpinShape(rand.nextInt(360), rand.nextInt(3), windowWidth, windowHeight, colorWheelOffset)); // rand.nextInt(3) causes color of shape to be random
						}
					}
					
					lastTime = System.currentTimeMillis();
					
					//score stuff
					currentScore+= elapsed;
					Message m = scoreHandler.obtainMessage();
					int formattedScore = (int)(currentScore/100.0);
					Bundle b = new Bundle();
					b.putString("text", "Score: "+formattedScore);
					b.putString("gameOverText", "Your score: "+formattedScore);
					m.setData(b);
					scoreHandler.sendMessage(m);
					
					for (int i = 0; i < shapes.size(); i++) {
						ColorSpinShape shape = shapes.get(i);
						int endState = shape.update(elapsed, Math.min(currentSpeed, 80), colorWheelAngle);
						if (endState == ColorSpinShape.STATE_BAD) {
							Message me = stateSetHandler.obtainMessage();
							Bundle bu = new Bundle();
							bu.putInt("state", STATE_GAME_OVER_TRANSITION);
							me.setData(bu);
							stateSetHandler.sendMessage(me);
						}
						if (shape.shouldPlaySound()) {
							playShapeSound();
						}
					}
				} //endif currentState == STATE_RUNNING
				else if (currentState == STATE_GAME_OVER_TRANSITION) {
					long elapsed = System.currentTimeMillis()-lastTime;
					
					boolean finished = true;
					for (int i = 0; i < shapes.size(); i++) {
						ColorSpinShape shape = shapes.get(i);
						shape.gameEnd();
						shape.update(elapsed, currentSpeed, colorWheelAngle);
						if (!shape.isDone()) {
							finished = false;
						}
					}
					if (finished) {
						Message me = stateSetHandler.obtainMessage();
						Bundle bu = new Bundle();
						bu.putInt("state", STATE_GAME_OVER);
						me.setData(bu);
						stateSetHandler.sendMessage(me);
					}
					lastTime = System.currentTimeMillis();
				}
				else if (currentState == STATE_MAIN_MENU) {
					long elapsed = System.currentTimeMillis() - lastTime;
					nextShapeTime+= elapsed;
					if (nextShapeTime > 2000) {
						nextShapeTime = 0;
						boolean updated = false;
						for (int i = 0; i < shapes.size(); i++) {
							ColorSpinShape shape = shapes.get(i);
							if (shape.isDone()) {
								shape.restartShape(rand.nextInt(360), rand.nextInt(3)); //second argument magic number allows the color to be randomized
								updated = true;
								break;
							}
						}
						if (!updated) {
							shapes.add(new ColorSpinShape(rand.nextInt(360), rand.nextInt(3), windowWidth, windowHeight, colorWheelOffset)); // rand.nextInt(3) causes color of shape to be random
						}
					}
					float nearestDistance = Float.MAX_VALUE;
					float destinationAngle=0;
					int nearestShapeColor=5; //triggers default in the switch if there are no shapes in the list
					for (int i = 0; i<shapes.size(); i++) {
						ColorSpinShape shape = shapes.get(i);
						if (!shape.isDone() && shape.getDistance() < nearestDistance && shape.getState() == ColorSpinShape.STATE_TRAVEL) {
							nearestDistance = shape.getDistance();
							destinationAngle = shape.getAngle();
							nearestShapeColor = shape.getColor();
						}
					}
					float millisTimeToArrival = (nearestDistance-colorWheelOffset*1.25f)*20; // nearestDistance/50pxpersecond*1000mspersecond
					switch (nearestShapeColor) {
					case ColorSpinShape.COLOR_YELLOW:
						destinationAngle+=60.0f;
						break;
					case ColorSpinShape.COLOR_BLUE:
						destinationAngle+=180.0f;
						break;
					case ColorSpinShape.COLOR_PURPLE:
						destinationAngle+=300.0f;
						break;
					default:
						break;
					}
					if (destinationAngle > 360) {
						destinationAngle -= 360;
					}
					float differenceInAngles = destinationAngle - colorWheelAngle;
					if (Math.abs(differenceInAngles) > 180) {
						float temp = 360 - Math.abs(differenceInAngles);
						if (differenceInAngles >= 0) {
							temp*=-1;
						}
						differenceInAngles = temp;
					}
					colorWheelAngle+= (differenceInAngles/millisTimeToArrival)*elapsed;
					while (colorWheelAngle > 360.0) {
						colorWheelAngle -= 360;
					}
					while (colorWheelAngle < 0.0) {
						colorWheelAngle += 360;
					}
					for (int i = 0; i<shapes.size(); i++) {
						ColorSpinShape shape = shapes.get(i);
						shape.update(elapsed, 50.0f, colorWheelAngle); //speed doesnt change in the menu
					}
					lastTime = System.currentTimeMillis();
				}
				else if (currentState == STATE_MAIN_MENU_TO_RUNNING_TRANSITION) {
					long elapsed = System.currentTimeMillis()-lastTime;
					mainMenuToRunningStateChangeTimer += elapsed;
					boolean finished = true;
					for (int i = 0; i < shapes.size(); i++) {
						ColorSpinShape shape = shapes.get(i);
						shape.gameEnd();
						shape.update(elapsed, 50.0f, colorWheelAngle);
						if (!shape.isDone()) {
							finished = false;
						}
					}
					if (mainMenuToRunningStateChangeTimer < 2000) {
						finished = false;
					}
					if (finished) {
						mainMenuToRunningStateChangeTimer = 0;
						shapes.clear();
						newGameReset();
						Message me = stateSetHandler.obtainMessage();
						Bundle bu = new Bundle();
						bu.putInt("state", STATE_RUNNING);
						me.setData(bu);
						stateSetHandler.sendMessage(me);
					}
					lastTime = System.currentTimeMillis();
				}
			}
			
		}
		
		public void doDraw(Canvas canvas) {
			//Log.e("activity", "DRAWING");
			canvas.drawColor(Color.WHITE);
			canvas.save();
			
			canvas.rotate(colorWheelAngle, windowWidth/2, windowHeight/2);
			if (windowWidth == 0) {
				setSurfaceSize(getWidth(), getHeight());
			}
			colorWheel.setBounds(windowWidth/2-colorWheelOffset, windowHeight/2-colorWheelOffset, windowWidth/2+colorWheelOffset, windowHeight/2+colorWheelOffset);
			
			Log.e("activity", "surface top left corner is "+(windowWidth/2-colorWheelOffset)+"x"+(windowHeight/2-colorWheelOffset));
			colorWheel.draw(canvas);
			canvas.restore();
			for (int i = 0; i < shapes.size(); i++) {
				ColorSpinShape shape = shapes.get(i);
				shape.draw(canvas);
			}
			
		}
		public void setRunning(boolean bool) {
			isRunning = bool;
		}
		
		public void newTouch(int type, float x, float y) {
			if (currentState != STATE_RUNNING) {				
				return;
			}
			synchronized (holder) {
				if (type == TOUCH_START) {
					touchX = (float) (x-0.5*windowWidth);
					lastTouchX = (float) (x-0.5*windowWidth);
					touchY = (float) (y-0.5*windowHeight);
					lastTouchY=(float) (y-0.5*windowHeight);
				}
				else {
					touchX = (float) (x-0.5*windowWidth);
					touchY = (float) (y-0.5*windowHeight);
				}
			}
			
		}
		
		public Bundle saveState() {
			Bundle b = new Bundle();
			b.putBoolean(KEY_NEW_SAVE, true);
			synchronized (holder) {
				b.putFloat(KEY_ANGLE, colorWheelAngle);
				b.putInt(KEY_STATE, currentState);
				String serialize = "";
				if (shapes.size() > 0) {
					serialize+=shapes.get(0).saveToString();	
					if (shapes.size() > 1) {
						for (int i = 1; i<shapes.size(); i++) {
							serialize+=";";
							serialize+=shapes.get(i).saveToString();
						}
					}
				}
				//Log.e("activity", "concatenated shape savestring="+serialize);
				b.putString(KEY_SHAPES, serialize);
				b.putLong(KEY_NEW_SHAPE_TIME, nextShapeTime);
				b.putFloat(KEY_SCORE, currentScore);
			}
			return b;
		}
		public void restoreState(Bundle save) {
			synchronized(holder) {
				setState(save.getInt(KEY_STATE, STATE_PAUSED));	
				if (currentState == STATE_PAUSED || currentState == STATE_GAME_OVER_TRANSITION) {
					colorWheelAngle = save.getFloat(KEY_ANGLE, colorWheelAngle);
					nextShapeTime = save.getLong(KEY_NEW_SHAPE_TIME, 2000);
					currentScore = save.getFloat(KEY_SCORE);
					String serializedShapes = save.getString(KEY_SHAPES);
					//Log.e("activity", "serialized restore string"+serializedShapes);
					String[] stringShapes = serializedShapes.split(";");
					//Log.e("activity", "serialized restore string split array length="+stringShapes.length);
					if (stringShapes != null) {
						for (String s : stringShapes) {
							if (s != null) {
								ColorSpinShape css = ColorSpinShape.restoreFromString(s);
								if (css != null) {
									shapes.add(css);
								}	
								else {
									setState(STATE_MAIN_MENU);
								}
							}
							else {
								setState(STATE_MAIN_MENU);
							}
						}
					}
				
				}
			}
		}
		public int getCurrentState() {
			synchronized(holder) {
				return currentState;
			}
		}
		public void newGameReset() {
			shapes.clear();
			colorWheelAngle=0;
			currentScore = 0;
			currentSpeed = 30.0f;
			nextShapeTime= 0;			
		}
	}
	
	public ColorSpinView(Context c, AttributeSet attrs) {
		super(c, attrs);
		mContext = c;
		//restore = b;
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		thread = new ColorSpinThread(holder, mContext, scoreViewHandler, stateHandler);
	}
	public void surfaceCreated(SurfaceHolder holder) {
		
		if (startRequested) {
			thread.start();
			startRequested = false;
			surfaceReady = false;
		}
		else {
			surfaceReady = true;
		}
		surfaceCreated = true;

		if (pauseMenuDownRequested) {//pausemenu stuff
			silentPauseMenuDown();
			pauseMenuDownRequested = false;
		}
		if (pauseMenuUpRequested) {
			silentPauseMenuUp();
			pauseMenuUpRequested = false;
		}
		if (mainMenuOutRequested) {//mainmenu stuff
			silentMainMenuOut();
			mainMenuOutRequested = false;
		}
		if (mainMenuInRequested) {
			silentMainMenuIn();
			mainMenuInRequested = false;
			
		}
		if (pauseButtonShowRequested) {
			silentShowPauseButton();
			pauseButtonShowRequested = false;
		}
		if (pauseButtonHideRequested) {
			silentHidePauseButton();
			pauseButtonHideRequested = false;
		}
		if (scoreViewShowRequested) {
			silentShowScoreView();
			scoreViewShowRequested = false;
		}
		if (scoreViewHideRequested) {
			silentHideScoreView();
			scoreViewHideRequested=  false;
		}
		if (gameOverMenuInRequested) {
			silentGameOverMenuIn();
			gameOverMenuInRequested = false;
		}
		if (gameOverMenuOutRequested) {
			silentGameOverMenuOut();
			gameOverMenuOutRequested = false;
		}
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		
		thread.setSurfaceSize(width, height);
		Log.e("activity", "surface size changed- dimensions are "+width+"x"+height);
	}
	public void surfaceDestroyed(SurfaceHolder holder) {
		surfaceCreated = false;
		//Log.e("activity", "surface destroyed");
		//release media players!
		releaseMediaPlayers();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN){
			thread.newTouch(ColorSpinThread.TOUCH_START, event.getX(), event.getY());
		}
		else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			thread.newTouch(ColorSpinThread.TOUCH_MOVE, event.getX(), event.getY());
		}
		return true;
	
	}
	public ColorSpinThread getThread() {
		return thread;
	}
	
	public void activityResume() {
		thread  = new ColorSpinThread(getHolder(), mContext, scoreViewHandler, stateHandler);
		initializeMediaPlayers();
	}
	public void saveThread(SharedPreferences.Editor editor) {
		Bundle b = thread.saveState();
		editor.putBoolean(KEY_NEW_SAVE, b.getBoolean(KEY_NEW_SAVE));
		editor.putFloat(KEY_ANGLE, b.getFloat(KEY_ANGLE));
		editor.putInt(KEY_STATE, b.getInt(KEY_STATE));
		editor.putString(KEY_SHAPES, b.getString(KEY_SHAPES));
		editor.putLong(KEY_NEW_SHAPE_TIME, b.getLong(KEY_NEW_SHAPE_TIME));
		editor.putFloat(KEY_SCORE, b.getFloat(KEY_SCORE));
		if (startMusicPlayer.getCurrentPosition() != 0) {
			editor.putInt(KEY_START_MUSIC_TIME, startMusicPlayer.getCurrentPosition());
			editor.putInt(KEY_LOOP_MUSIC_TIME, 0);
		}
		else if (loopMusicPlayer.getCurrentPosition() != 0) {
			editor.putInt(KEY_LOOP_MUSIC_TIME, loopMusicPlayer.getCurrentPosition());
			editor.putInt(KEY_START_MUSIC_TIME, 0);
		}
		editor.putBoolean(KEY_SOUND_MUTED, soundMuted);
		editor.putBoolean(KEY_MUSIC_MUTED, musicMuted);
		editor.commit();
	}
	public void restoreThread(SharedPreferences prefs) {
		Bundle b = new Bundle();
		b.putFloat(KEY_ANGLE, prefs.getFloat(KEY_ANGLE, 0));
		b.putInt(KEY_STATE, prefs.getInt(KEY_STATE, 0));
		b.putString(KEY_SHAPES, prefs.getString(KEY_SHAPES, null));
		b.putLong(KEY_NEW_SHAPE_TIME, prefs.getLong(KEY_NEW_SHAPE_TIME, 2000));
		b.putFloat(KEY_SCORE, prefs.getFloat(KEY_SCORE, 0));
		thread.restoreState(b);
		if (prefs.getInt(KEY_START_MUSIC_TIME, 1) != 0) {
			startMusicPlayer.seekTo(prefs.getInt(KEY_START_MUSIC_TIME, 1));
		}
		else {
			loopMusicPlayer.seekTo(prefs.getInt(KEY_LOOP_MUSIC_TIME, 1));
		}
		if (prefs.getBoolean(KEY_MUSIC_MUTED, false)){
			setMusicMute(true);
		}
		if (prefs.getBoolean(KEY_SOUND_MUTED, false)) {
			setSoundMute(true);
		}
	}
	public void requestThreadStart() {
		if (surfaceReady) {
			thread.start();
			surfaceReady = false;
			startRequested = false;
		}
		else {
			startRequested = true;
		}
	}
	public void setSoundControls(View v) {
		soundControls = v;
		soundControlsWrapper = new LayoutParamsWrapper((MarginLayoutParams) v.getLayoutParams());
	}
	public void setPauseMenu(View v) {
		pauseMenu = v;
		pauseMenuWrapper = new LayoutParamsWrapper((MarginLayoutParams) v.getLayoutParams());
	}
	public void setMainMenu(View v) {
		mainMenu = v;
		mainMenuWrapper = new LayoutParamsWrapper((MarginLayoutParams) v.getLayoutParams());
	}
	public void setPauseButton(View v) {
		pauseButton = v;
		pauseButtonWrapper = new LayoutParamsWrapper((MarginLayoutParams) v.getLayoutParams());
	}
	public void setScoreView(TextView t) {
		scoreView = t;
		scoreViewWrapper = new LayoutParamsWrapper((MarginLayoutParams) t.getLayoutParams());
	}

	public void setGameOverMenu(View v) {
		gameOverMenu = v;
		gameOverMenuWrapper = new LayoutParamsWrapper((MarginLayoutParams) v.getLayoutParams());
	}

	public void setGameOverScoreView(TextView t) {
		gameOverScoreView = t;
	
	}

	public void silentPauseMenuUp() {
		if (Build.VERSION.SDK_INT >= 11) {
			pauseMenu.setVisibility(View.GONE);
	    	ObjectAnimator.ofFloat(pauseMenu, "translationY", 0).setDuration(1).start();
	    	pauseMenu.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseMenuWrapper, "topMargin", pauseMenuWrapper.getInitialTopMargin()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(pauseMenu));
			oa.start();
		}
	}

	public void silentPauseMenuDown() {
		if (Build.VERSION.SDK_INT >= 11) {//honeycomb uses animation api
	    	pauseMenu.setVisibility(View.GONE);
	    	ObjectAnimator.ofFloat(pauseMenu, "translationY", 2*pauseMenu.getHeight()).setDuration(1).start();
	    	pauseMenu.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseMenuWrapper, "topMargin", pauseMenuWrapper.getInitialTopMargin()+2*pauseMenu.getHeight()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(pauseMenu));
			oa.start();
		}
    }
	public void silentMainMenuIn() {
		if (Build.VERSION.SDK_INT >= 11) {
			mainMenu.setVisibility(View.GONE);
	    	ObjectAnimator.ofFloat(mainMenu, "translationX", 0).setDuration(1).start();
	    	mainMenu.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(mainMenuWrapper, "leftMargin", mainMenuWrapper.getInitialLeftMargin()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(mainMenu));
			oa.start();
		}
	}

	public void silentMainMenuOut() {
		if (Build.VERSION.SDK_INT >= 11) {
			mainMenu.setVisibility(View.GONE);
	    	ObjectAnimator.ofFloat(mainMenu, "translationX", -2*mainMenu.getWidth()).setDuration(1).start();
	    	mainMenu.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(mainMenuWrapper, "leftMargin", mainMenuWrapper.getInitialLeftMargin()-2*mainMenu.getWidth()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(mainMenu));
			oa.start();
		}
	
	}
	
	public void silentShowPauseButton() {
		if (Build.VERSION.SDK_INT >= 11) {
			pauseButton.setVisibility(View.GONE);
			ObjectAnimator.ofFloat(pauseButton, "translationY", 0).setDuration(1).start();
			pauseButton.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseButtonWrapper, "topMargin", pauseButtonWrapper.getInitialTopMargin()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(pauseButton));
			oa.start();
		}
	}

	public void silentHidePauseButton() {
		if (Build.VERSION.SDK_INT >= 11) {
			pauseButton.setVisibility(View.GONE);
			ObjectAnimator.ofFloat(pauseButton, "translationY", -2*pauseButton.getHeight()).setDuration(1).start();
			pauseButton.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseButtonWrapper, "topMargin", pauseButtonWrapper.getInitialTopMargin()-2*pauseButton.getHeight()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(pauseButton));
			oa.start();
		}
	}

	public void silentShowScoreView() {
		if (Build.VERSION.SDK_INT >= 11) {
			scoreView.setVisibility(View.GONE);
			ObjectAnimator.ofFloat(scoreView, "translationY", 0).setDuration(1).start();
			scoreView.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(scoreViewWrapper, "topMargin", scoreViewWrapper.getInitialTopMargin()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(scoreView));
			oa.start();
		}
	}

	public void silentHideScoreView() {
		if (Build.VERSION.SDK_INT >= 11) {
			scoreView.setVisibility(View.GONE);
			ObjectAnimator.ofFloat(scoreView, "translationY", -scoreView.getHeight()).setDuration(1).start();
			scoreView.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(scoreViewWrapper, "topMargin", scoreViewWrapper.getInitialTopMargin()-scoreView.getHeight()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(scoreView));
			oa.start();
		}
	}

	public void silentGameOverMenuIn() {
		if (Build.VERSION.SDK_INT >= 11) {
			gameOverMenu.setVisibility(View.GONE);
			ObjectAnimator.ofFloat(gameOverMenu, "translationY", 0).setDuration(1).start();
			gameOverMenu.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(gameOverMenuWrapper, "topMargin", gameOverMenuWrapper.getInitialTopMargin()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(gameOverMenu));
			oa.start();
		}
	}

	public void silentGameOverMenuOut() {
		if (Build.VERSION.SDK_INT >= 11) {
			gameOverMenu.setVisibility(View.GONE);
			ObjectAnimator.ofFloat(gameOverMenu, "translationY", -3*gameOverMenu.getHeight()).setDuration(1).start();
			gameOverMenu.setVisibility(View.VISIBLE);
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(gameOverMenuWrapper, "topMargin", gameOverMenuWrapper.getInitialTopMargin()-2*gameOverMenu.getHeight()).setDuration(1);
			//Log.e("activity", "!!!!!!!!!!!!!!!!1silently moved game over menu out");
			oa.addUpdateListener(new AnimationInvalidator(gameOverMenu));
			oa.start();
		}
	}

	public void pauseMenuUp() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseMenu, "translationY", 0).setDuration(1000);
			oa.addListener(new ChildDisabler(pauseMenu));
			oa.start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseMenuWrapper, "topMargin", pauseMenuWrapper.getInitialTopMargin()).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(pauseMenu));
			oa.addListener(new ChildDisabler(pauseMenu));
			oa.start();
		}
	}
	public void pauseMenuDown() {
		if (Build.VERSION.SDK_INT >= 11) {//honeycomb uses animation api
	    	ObjectAnimator oa = ObjectAnimator.ofFloat(pauseMenu, "translationY", 2*pauseMenu.getHeight()).setDuration(1000);
	    	oa.addListener(new ChildDisabler(pauseMenu));
	    	oa.start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseMenuWrapper, "topMargin", pauseMenuWrapper.getInitialTopMargin()+2*pauseMenu.getHeight()).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(pauseMenu));
			oa.addListener(new ChildDisabler(pauseMenu));
			oa.start();
		}
	}
	public void showSoundControls() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator oa = ObjectAnimator.ofFloat(soundControls, "translationY", 0).setDuration(1000);
			oa.addListener(new ChildDisabler(soundControls));
			oa.start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(soundControls, "translationY", 0).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(soundControls));
			oa.addListener(new ChildDisabler(soundControls));
			oa.start();
		}
	}
	public void hideSoundControls() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator oa = ObjectAnimator.ofFloat(soundControls, "translationY", 3*soundControls.getHeight()).setDuration(1000);
	    	oa.addListener(new ChildDisabler(soundControls));
	    	oa.start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(soundControlsWrapper, "bottomMargin", soundControlsWrapper.getInitialTopMargin()-2*soundControls.getHeight()).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(soundControls));
			oa.addListener(new ChildDisabler(soundControls));
			oa.start();
		}
	}
	public void mainMenuIn() {
		if (Build.VERSION.SDK_INT >= 11) {
	    	ObjectAnimator oa = ObjectAnimator.ofFloat(mainMenu, "translationX", 0).setDuration(1000);
	    	oa.addListener(new ChildDisabler(mainMenu));
	    	oa.start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(mainMenuWrapper, "leftMargin", mainMenuWrapper.getInitialLeftMargin()).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(mainMenu));
			oa.addListener(new ChildDisabler(mainMenu));
			oa.start();
		}
	}
	public void mainMenuOut() {
		if (Build.VERSION.SDK_INT >= 11) {
        	ObjectAnimator oa = ObjectAnimator.ofFloat(mainMenu, "translationX", -2*mainMenu.getWidth()).setDuration(1000);
        	oa.addListener(new ChildDisabler(mainMenu));
        	oa.start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(mainMenuWrapper, "leftMargin", mainMenuWrapper.getInitialLeftMargin()-2*mainMenu.getWidth()).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(mainMenu));
			oa.addListener(new ChildDisabler(mainMenu));
			oa.start();
		}
	}
	public void showPauseButton() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator.ofFloat(pauseButton, "translationY", 0).setDuration(1).start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseButtonWrapper, "topMargin", pauseButtonWrapper.getInitialTopMargin()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(pauseButton));
			oa.start();
		}
	}
	public void hidePauseButton() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator.ofFloat(pauseButton, "translationY", -2*pauseButton.getHeight()).setDuration(1).start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(pauseButtonWrapper, "topMargin", pauseButtonWrapper.getInitialTopMargin()-2*pauseButton.getHeight()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(pauseButton));
			oa.start();
		}
	}
	public void showScoreView() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator.ofFloat(scoreView, "translationY", 0).setDuration(1).start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(scoreViewWrapper, "topMargin", scoreViewWrapper.getInitialTopMargin()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(scoreView));
			oa.start();
		}
	}
	public void hideScoreView() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator.ofFloat(scoreView, "translationY", -scoreView.getHeight()).setDuration(1).start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(scoreViewWrapper, "topMargin", scoreViewWrapper.getInitialTopMargin()-scoreView.getHeight()).setDuration(1);
			oa.addUpdateListener(new AnimationInvalidator(scoreView));
			oa.start();
		}
	}
	public void gameOverMenuIn() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator oa = ObjectAnimator.ofFloat(gameOverMenu, "translationY", 0).setDuration(1000);
			oa.addListener(new ChildDisabler(gameOverMenu));
			oa.start();
		}
		else {
			ObjectAnimator oa = ObjectAnimator.ofFloat(gameOverMenuWrapper, "topMargin", gameOverMenuWrapper.getInitialTopMargin()).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(gameOverMenu));
			oa.addListener(new ChildDisabler(gameOverMenu));
			oa.start();
		}
	}
	public void gameOverMenuOut() {
		if (Build.VERSION.SDK_INT >= 11) {
			ObjectAnimator oa = ObjectAnimator.ofFloat(gameOverMenu, "translationY", -3*gameOverMenu.getHeight()).setDuration(1000);
			oa.addListener(new ChildDisabler(gameOverMenu));
			oa.start();
		}
		else {
			//Log.e("activity", "gameOverMenu.getHeight = "+gameOverMenu.getHeight()+"margin-"+gameOverMenuWrapper.getInitialTopMargin());
			ObjectAnimator oa = ObjectAnimator.ofFloat(gameOverMenuWrapper, "topMargin", gameOverMenuWrapper.getInitialTopMargin()-2*gameOverMenu.getHeight()).setDuration(1000);
			oa.addUpdateListener(new AnimationInvalidator(gameOverMenu));
			oa.addListener(new ChildDisabler(gameOverMenu));
			oa.start();
		}
	}
	
	private class AnimationInvalidator implements ValueAnimator.AnimatorUpdateListener{
		
		
		View view;
		public AnimationInvalidator(View v) {
			view = v;
			
		}
		
		@Override
		public void onAnimationUpdate(ValueAnimator animation) {
			view.requestLayout();
			view.invalidate();
			if (view instanceof ViewGroup) {
				
				ViewGroup group = (ViewGroup) view;
				for (int i = 0; i < group.getChildCount(); i++) {
					View child = group.getChildAt(i);
					child.invalidate();
				}
			}
		}	
	}
	private class ChildDisabler implements Animator.AnimatorListener {
		
		View view;
		public ChildDisabler(View v) {
			view = v;
		}
		@Override
		public void onAnimationStart(Animator animation) {
			if (view instanceof ViewGroup) {
				
				ViewGroup group = (ViewGroup) view;
				for (int i = 0; i < group.getChildCount(); i++) {
					View child = group.getChildAt(i);
					child.setClickable(false);
				}
			}
			
		}

		@Override
		public void onAnimationEnd(Animator animation) {
			if (view instanceof ViewGroup) {
				
				ViewGroup group = (ViewGroup) view;
				for (int i = 0; i < group.getChildCount(); i++) {
					View child = group.getChildAt(i);
					child.setClickable(true);
				}
			}
			
			
		}

		@Override
		public void onAnimationCancel(Animator animation) {
			
			
		}

		@Override
		public void onAnimationRepeat(Animator animation) {
			
			
		}
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		Log.e("activity", "reached onAudiofocuschange");
		switch (focusChange) {
        case AudioManager.AUDIOFOCUS_GAIN:
        	Log.e("activity", "We got the focus yay");
            hasAudioFocus = true;
            if (threadWantsMusic) {
            	startMusic();
            }
            break;

        case AudioManager.AUDIOFOCUS_LOSS:
        	hasAudioFocus = false;
        	releaseMediaPlayers();
        	break;
            
        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
        	hasAudioFocus = false;
        	startMusicPlayer.pause();
        	loopMusicPlayer.pause();
        	endNoisePlayer.pause();
        	shapeSoundPlayer.pause();
        	break;           

        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
        	hasAudioFocus = false;
        	startMusicPlayer.pause();
        	loopMusicPlayer.pause();
        	endNoisePlayer.pause();
        	shapeSoundPlayer.pause();
        	break;
            
		}
		
	}
	public void startMusic() {
		threadWantsMusic = true;
		Log.e("activity", "the thread wants music, hasAudioFocus="+hasAudioFocus+" and mediaPlayersInitialized="+mediaPlayersInitialized);
		if (hasAudioFocus && mediaPlayersInitialized) {
			if (loopMusicPlayer.getCurrentPosition() != 0) {
				loopMusicPlayer.start();
			}
			else {
				startMusicPlayer.start();
			}
			Log.e("activity", "started the Music!!!");
		}
	}
	public void endMusicWithSweep() {
		threadWantsMusic = false;
		if (hasAudioFocus && mediaPlayersInitialized) {
			if (startMusicPlayer.isPlaying()) {
				startMusicPlayer.pause();
				startMusicPlayer.seekTo(0);
			}
			else if (loopMusicPlayer.isPlaying()) {
				loopMusicPlayer.pause();
				loopMusicPlayer.seekTo(0);
			}
			endNoisePlayer.start();
		}
	}
	public void endMusicWithoutSweep() {
		threadWantsMusic = false;
		if (hasAudioFocus && mediaPlayersInitialized) {
			if (startMusicPlayer.isPlaying()) {
				startMusicPlayer.pause();
				startMusicPlayer.seekTo(0);
			}
			else if (loopMusicPlayer.isPlaying()) {
				loopMusicPlayer.pause();
				loopMusicPlayer.seekTo(0);
			}
		}
	}
	public void pauseMusicWithoutSweep() {
		threadWantsMusic = false;
		if (hasAudioFocus && mediaPlayersInitialized) {
			if (startMusicPlayer.isPlaying()) {
				startMusicPlayer.pause();
			}
			else if (loopMusicPlayer.isPlaying()) {
				loopMusicPlayer.pause();
			}
		}
	}
	public void resetMusicToBeginning() {
		if (hasAudioFocus && mediaPlayersInitialized) {
			startMusicPlayer.seekTo(0);
			loopMusicPlayer.seekTo(0);
		}
	}
	public void playShapeSound() {
		if (hasAudioFocus && mediaPlayersInitialized) {
			shapeSoundPlayer.seekTo(0);
			shapeSoundPlayer.start();
		}
	}
	public void setMusicMute(boolean muted) {
		musicMuted = muted;
		if (mediaPlayersInitialized) {
			if (muted) {
				startMusicPlayer.setVolume(0f, 0f);
				loopMusicPlayer.setVolume(0f, 0f);
				endNoisePlayer.setVolume(0f, 0f);
			}
			else {
				startMusicPlayer.setVolume(1f, 1f);
				loopMusicPlayer.setVolume(1f, 1f);
				endNoisePlayer.setVolume(1f, 1f);
			}
		}
	}
	public void setSoundMute(boolean muted) {
		soundMuted = muted;
		if (mediaPlayersInitialized) {
			if (muted) {
				shapeSoundPlayer.setVolume(0f, 0f);
			}
			else {
				shapeSoundPlayer.setVolume(1f, 1f);
			}
		}
	}
	public void initializeMediaPlayers() {
		startMusicPlayer = MediaPlayer.create(mContext, R.raw.start_theme);
		loopMusicPlayer = MediaPlayer.create(mContext, R.raw.loop);
		endNoisePlayer = MediaPlayer.create(mContext, R.raw.end_sweep);
		shapeSoundPlayer = MediaPlayer.create(mContext, R.raw.shape_enter);
		startMusicPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				loopMusicPlayer.seekTo(0);
				loopMusicPlayer.start();
			}
		});
		loopMusicPlayer.setLooping(true);
		startMusicPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		loopMusicPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		endNoisePlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		shapeSoundPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mediaPlayersInitialized = true;
	}
	public void releaseMediaPlayers() {
		if (mediaPlayersInitialized) {
			Log.e("activity", "releasing media players!!!");
			mediaPlayersInitialized = false;
			startMusicPlayer.release();
			loopMusicPlayer.release();
			endNoisePlayer.release();
			shapeSoundPlayer.release();
			startMusicPlayer = null;
			loopMusicPlayer = null;
			endNoisePlayer = null;
			shapeSoundPlayer = null;
		}
	}
}
